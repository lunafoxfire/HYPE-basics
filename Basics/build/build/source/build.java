import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.behavior.HRotate; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {




HRect d1,d2,d3,d4,d5;

public void setup() {
  
  H.init(this).background(0xff666666);

  d1 = new HRect();
  d1.size(50)
    .rotation(45)
    .anchor(5, 5)
    .loc(100, height/2)
    .fill((int)random(255), (int)random(255), (int)random(255));
  HRotate r1 = new HRotate().target(d1).speed(1);
  H.add(d1);

  d2 = new HRect();
  d2.size(50)
    .rotation(45)
    .anchor(10, 10)
    .loc(200, height/2)
    .fill((int)random(255), (int)random(255), (int)random(255));
  HRotate r2 = new HRotate().target(d2).speed(2);
  H.add(d2);

  d3 = new HRect();
  d3.size(50)
    .rotation(45)
    .anchor(15, 15)
    .loc(300, height/2)
    .fill((int)random(255), (int)random(255), (int)random(255));
  HRotate r3 = new HRotate().target(d3).speed(3);
  H.add(d3);

  d4 = new HRect();
  d4.size(50)
    .rotation(45)
    .anchor(25, 25)
    .loc(400, height/2)
    .fill((int)random(255), (int)random(255), (int)random(255));
  HRotate r4 = new HRotate().target(d4).speed(4);
  H.add(d4);

  d5 = new HRect();
  d5.size(50)
    .rotation(45)
    .anchor(25, -25)
    .loc(500, height/2)
    .fill((int)random(255), (int)random(255), (int)random(255));
  HRotate r5 = new HRotate().target(d5).speed(999);
  H.add(d5);
}

public void draw() {
  H.drawStage();

  line(0, height/2, width, height/2);

}
  public void settings() {  size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
