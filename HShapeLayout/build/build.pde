import hype.*;
import hype.extended.colorist.HColorPool;
import hype.extended.layout.HShapeLayout;

HColorPool colors;
HDrawablePool pool;

void setup() {
  size(600, 600);
  H.init(this).background(#181818);
  smooth();

  colors = new HColorPool()
    .add(#ffffff)
    .add(#f7f7f7)
    .add(#ececec)
    .add(#333333, 3)
    .add(#0095a8, 2)
    .add(#00616f, 2)
    .add(#ff3300, 2)
    .add(#ff6600, 2)
  ;

  pool = new HDrawablePool(2000);
  pool.autoAddToStage()
    // .add(new HShape("svg1.svg"))
    // .add(new HShape("svg2.svg"))
    // .add(new HShape("svg3.svg"))
    // .add(new HShape("svg4.svg"))
    .add(new HShape("svg5.svg"))
    // .add(new HShape("svg6.svg"))

    .layout(
      new HShapeLayout()
        .target(
            new HImage("shape-map.png")
        )
    )

    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .noStroke()
              .anchorAt(H.CENTER)
              .size((int)random(10, 30))
              .rotate((int)random(360))
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
  noLoop();
}

void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
