import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorPool; 
import hype.extended.layout.HShapeLayout; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {





HColorPool colors;
HDrawablePool pool;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  colors = new HColorPool()
    .add(0xffffffff)
    .add(0xfff7f7f7)
    .add(0xffececec)
    .add(0xff333333, 3)
    .add(0xff0095a8, 2)
    .add(0xff00616f, 2)
    .add(0xffff3300, 2)
    .add(0xffff6600, 2)
  ;

  pool = new HDrawablePool(2000);
  pool.autoAddToStage()
    // .add(new HShape("svg1.svg"))
    // .add(new HShape("svg2.svg"))
    // .add(new HShape("svg3.svg"))
    // .add(new HShape("svg4.svg"))
    .add(new HShape("svg5.svg"))
    // .add(new HShape("svg6.svg"))

    .layout(
      new HShapeLayout()
        .target(
            new HImage("shape-map.png")
        )
    )

    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .noStroke()
              .anchorAt(H.CENTER)
              .size((int)random(10, 30))
              .rotate((int)random(360))
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
  noLoop();
}

public void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

public void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
  public void settings() {  size(600, 600);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
