import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorPool; 
import hype.extended.layout.HGridLayout; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {





HColorPool colors;
HDrawablePool pool;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  colors = new HColorPool()
    .add(0xffffffff)
    .add(0xfff7f7f7)
    .add(0xffececec)
    .add(0xff333333, 3)
    .add(0xff0095a8, 2)
    .add(0xff00616f, 2)
    .add(0xffff3300, 2)
    .add(0xffff6600, 2)
  ;

  pool = new HDrawablePool(121);
  pool.autoAddToStage()
    .add(new HShape("svg1.svg"))
    .add(new HShape("svg2.svg"))
    .add(new HShape("svg3.svg"))
    .add(new HShape("svg4.svg"))
    .add(new HShape("svg5.svg"))
    .add(new HShape("svg6.svg"))

    .layout(
      new HGridLayout()
        .startX(25)
        .startY(25)
        .spacing(50, 50)
        .cols(11)
    )

    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(1)
              .stroke(0xff000000)
              .anchorAt(H.CENTER)
              .rotate((int)random(4) * 90)
              .size( 50 + (int)random(2) * 50)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
}
  public void settings() {  size(1280, 720);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
