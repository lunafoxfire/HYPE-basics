import hype.*;
import hype.extended.colorist.HColorPool;
import hype.extended.layout.HGridLayout;

HColorPool colors;
HDrawablePool pool;

void setup() {
  size(1280, 720);
  H.init(this).background(#181818);
  smooth();

  colors = new HColorPool()
    .add(#ffffff)
    .add(#f7f7f7)
    .add(#ececec)
    .add(#333333, 3)
    .add(#0095a8, 2)
    .add(#00616f, 2)
    .add(#ff3300, 2)
    .add(#ff6600, 2)
  ;

  pool = new HDrawablePool(121);
  pool.autoAddToStage()
    .add(new HShape("svg1.svg"))
    .add(new HShape("svg2.svg"))
    .add(new HShape("svg3.svg"))
    .add(new HShape("svg4.svg"))
    .add(new HShape("svg5.svg"))
    .add(new HShape("svg6.svg"))

    .layout(
      new HGridLayout()
        .startX(25)
        .startY(25)
        .spacing(50, 50)
        .cols(11)
    )

    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(1)
              .stroke(#000000)
              .anchorAt(H.CENTER)
              .rotate((int)random(4) * 90)
              .size( 50 + (int)random(2) * 50)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
}
