import hype.*;
import hype.extended.colorist.HColorPool;

HColorPool colors;
HDrawablePool pool;

void setup() {
  size(1280, 720);
  H.init(this).background(#181818);
  smooth();

  colors = new HColorPool()
    .add(#ffffff)
    .add(#f7f7f7)
    .add(#ececec)
    .add(#333333, 3)
    .add(#0095a8, 2)
    .add(#00616f, 2)
    .add(#ff3300, 2)
    .add(#ff6600, 2)
  ;

  pool = new HDrawablePool(50);
  pool.autoAddToStage()
    .add(new HShape("mongo1.svg"))
    .add(new HShape("mongo2.svg"))
    .add(new HShape("mongo3.svg"))
    .add(new HShape("mongo4.svg"))
    .add(new HShape("mongo5.svg"))
    .add(new HShape("mongo6.svg"))
    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(1)
              .stroke(#000000)
              .size((int)random(150, 400))
              .rotate((int)random(360))
              .loc((int)random(width), (int)random(height))
              .anchorAt(H.CENTER)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();

  H.drawStage();
}
