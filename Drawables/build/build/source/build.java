import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {



public void setup() {
  
  H.init(this).background(0xff181818);
  

  for(int i = 0; i < 150; i++) {
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .strokeWeight(1)
      .stroke(0xffff3300)
      .fill(0xff111111, 64)
      .size((int)random(25, 125), (int)random(25, 125))
      .rotate((int)random(360))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

public void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

public void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
  public void settings() {  size(1280, 720);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
