import hype.*;
import hype.extended.colorist.HPixelColorist;

HPixelColorist colors;
HPixelColorist inverseColors;

void setup() {
  size(1280, 720);
  H.init(this).background(#181818);
  smooth();

  H.add(new HImage("toriel.png"));

  colors = new HPixelColorist("toriel.png").fillAndStroke();
  inverseColors = new HPixelColorist("toriel_invert.png").fillAndStroke();

  for(int i = 0; i < 150; i++) {
    int size = (int)random(25, 100);
    int orientation = 25;
    HRect d = new HRect();
    d
      .rounding(((int)random(-100, -10)))
      .strokeWeight(1)
      .alpha(150)
      .size(size, size + (int)random(0, 80))
      .rotate(orientation + (int)random(-5, 5))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    // colors.applyColor(d);
    inverseColors.applyColor(d);
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
