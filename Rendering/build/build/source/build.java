import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorPool; 
import hype.extended.layout.HGridLayout; 
import processing.pdf.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {






int gridSize = 50;
int rows = 15;
int cols = 25;
HColorPool colors;
HDrawablePool pool;

public void settings() {
  size(gridSize * cols, gridSize * rows);
}

public void setup() {
  H.init(this).background(0xff202020);
  
  colors = new HColorPool()
    .add(0xffffffff)
    .add(0xfff7f7f7)
    .add(0xffececec)
    .add(0xff333333, 3)
    .add(0xff0095a8, 2)
    .add(0xff00616f, 2)
    .add(0xffff3300, 2)
    .add(0xffff6600, 2)
  ;
  pool = new HDrawablePool(rows * cols);
  pool.autoAddToStage()
    .add(new HShape("svg1.svg"))
    .add(new HShape("svg2.svg"))
    // .add(new HShape("svg3.svg"))
    // .add(new HShape("svg4.svg"))
    // .add(new HShape("svg5.svg"))
    // .add(new HShape("svg6.svg"))
    .layout(
      new HGridLayout()
        .startX(gridSize / 2)
        .startY(gridSize / 2)
        .spacing(gridSize, gridSize)
        .cols(cols)
    )
    .onCreate(
        new HCallback() {
          public void run(Object obj) {
            HShape d = (HShape)obj;
            d
              .enableStyle(false)
              .strokeJoin(ROUND)
              .strokeCap(ROUND)
              .strokeWeight(0)
              .stroke(0xff000000)
              .anchorAt(H.CENTER)
              .rotate((int)random(4) * 90)
              .size(50)
            ;
            d.randomColors(colors.fillOnly());
          }
        }
      )
    .requestAll();
    noLoop();
}

public void draw() {
  H.drawStage();
}

public void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

// vector
public void saveVector() {
  PGraphics tmp = null;
  tmp = beginRecord(PDF, "render/render.pdf");
    if (tmp == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(tmp, false, 1);
    }
  endRecord();
}

// raster image
public void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
