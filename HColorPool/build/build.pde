import hype.*;
import hype.extended.colorist.HColorPool;

HColorPool colors;

void setup() {
  size(400, 1000);
  H.init(this).background(#181818);
  smooth();

  colors = new HColorPool()
    .add(#555555, 5)
    .add(#111111, 5)
    .add(#8EB922)
    .add(#CBFF48)
    .add(#C4FF2F)
    .add(#7F00B2, 3)
    .add(#C32FFF, 3)
  ;

  for(int i = 0; i < 100; i++) {
    int size = (int)random(20, 80);
    HRect d = (HRect) new HRect()
      .rounding(3)
      .strokeWeight(4)
      .stroke(#000000)
      .fill(colors.getColor(), 210)
      .size(size, size + (int)random(80))
      .rotate((int)random(-20, 20))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
