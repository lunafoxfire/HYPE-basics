import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hype.*; 
import hype.extended.colorist.HColorPool; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class build extends PApplet {




HColorPool colors;

public void setup() {
  
  H.init(this).background(0xff181818);
  

  colors = new HColorPool()
    .add(0xff555555, 5)
    .add(0xff111111, 5)
    .add(0xff8EB922)
    .add(0xffCBFF48)
    .add(0xffC4FF2F)
    .add(0xff7F00B2, 3)
    .add(0xffC32FFF, 3)
  ;

  for(int i = 0; i < 100; i++) {
    int size = (int)random(20, 80);
    HRect d = (HRect) new HRect()
      .rounding(3)
      .strokeWeight(4)
      .stroke(0xff000000)
      .fill(colors.getColor(), 210)
      .size(size, size + (int)random(80))
      .rotate((int)random(-20, 20))
      .loc((int)random(width), (int)random(height))
      .anchorAt(H.CENTER)
    ;
    H.add(d);
  }

  H.drawStage();
  noLoop();
}

public void keyPressed() {
  if (key == 's') {
    saveHiRes(2);
  }
}

public void saveHiRes(int scale) {
  PGraphics hires = createGraphics(width * scale, height * scale, JAVA2D);
  beginRecord(hires);
    hires.scale(scale);
    if (hires == null) {
      H.drawStage();
    }
    else {
      H.stage().paintAll(hires, false, 1);
    }
  endRecord();
  hires.save("render/render.png");
}
  public void settings() {  size(400, 1000);  smooth(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "build" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
